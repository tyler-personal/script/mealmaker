package com.tyler.mealmaker.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView

fun TextView.onChangeListener(block: (old: String, new: String) -> Unit) {
    var before = ""
    var after = ""
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            s?.let { after = it.toString() }
            block(before, after)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            s?.let { before = it.toString() }
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

val TextView.textString get() = text.toString()
val TextView.textNumber get() = text.toString().toInt()