package com.tyler.mealmaker.extensions

import org.atteo.evo.inflector.English
import com.wolfram.alpha.WAQuery
import com.wolfram.alpha.impl.WAQueryImpl

fun String.pluralize() = English.plural(this).capitalize()
