package com.tyler.mealmaker.extensions

import android.view.View
import android.view.ViewGroup

fun View.setMargin(left: Int? = null, right: Int? = null, top: Int? = null, bottom: Int? = null) {
    layoutParams = (layoutParams as ViewGroup.MarginLayoutParams).apply {
        setMargins(left ?: leftMargin, top ?: topMargin, right ?: rightMargin, bottom ?: bottomMargin)
    }
}