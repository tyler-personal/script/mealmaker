package com.tyler.mealmaker.extensions

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.tyler.mealmaker.R

fun Spinner.setOptions(context: Context, options: List<String>) {
    adapter = ArrayAdapter<String>(context, R.layout.spinner_add_item, options).apply {
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    }
}
fun Spinner.setText(text: String): Boolean {
    0.until(adapter.count).find { adapter.getItem(it) == text }?.let {
        setSelection(it)
    } ?: return false
    return true
}
