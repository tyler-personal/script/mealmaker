package com.tyler.mealmaker.entities

data class UnitOfMeasure(val short: String, val singular: String, val plural: String) {
    constructor(short: String, singular: String): this(short, singular, "${singular}s")

    companion object {
        fun get(name: String): UnitOfMeasure? = all
            .flatMap { listOf(it.singular to it, it.plural to it) }
            .toMap()[name]

        val all: List<UnitOfMeasure> = massList() + volumeList() + lengthList()

        val singulars: List<String> = all.map { it.singular }
        val plurals: List<String> = all.map { it.plural }

        fun massList() = listOf(
            UnitOfMeasure("lb", "pound"),
            UnitOfMeasure("oz", "ounce"),
            UnitOfMeasure("mg", "milligram"),
            UnitOfMeasure("g", "gram"),
            UnitOfMeasure("kg", "kilogram")
        )

        fun volumeList() = listOf(
            UnitOfMeasure("tsp", "teaspoon"),
            UnitOfMeasure("tbsp", "tablespoon"),
            UnitOfMeasure("fl oz", "fluid ounce"),
            UnitOfMeasure("c", "cup"),
            UnitOfMeasure("pt", "pint"),
            UnitOfMeasure("qt", "quart"),
            UnitOfMeasure("gal", "gallon"),
            UnitOfMeasure("ml", "milliliter"),
            UnitOfMeasure("l", "liter")
        )
        fun lengthList() = listOf(
            UnitOfMeasure("mm", "millimeter"),
            UnitOfMeasure("cm", "centimeter"),
            UnitOfMeasure("m", "meter"),
            UnitOfMeasure("in", "inch", "inches"),
            UnitOfMeasure("ft","foot", "feet")
        )
    }
}