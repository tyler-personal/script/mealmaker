package com.tyler.mealmaker.entities

data class Item(val name: String, val count: Int, val unit: UnitOfMeasure) {
    val pluralizedUnit get() =if (count == 1) unit.singular else unit.plural

    override fun toString() = "$name: $count $pluralizedUnit"
}