package com.tyler.mealmaker

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu

import kotlinx.android.synthetic.main.activity_main.*

import com.tyler.mealmaker.helpers.makeSnackbar
import com.tyler.mealmaker.views.AddItemModal
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        add_item.setOnClickListener { listenerView ->
            AddItemModal(this) { view, item ->
                scroll_layout.addView(view)
                makeSnackbar(listenerView, item.name)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu) = true.also { menuInflater.inflate(R.menu.menu_main, menu) }
}

