package com.tyler.mealmaker.views

import android.app.AlertDialog
import android.content.Context
import android.icu.text.PluralFormat
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.github.plural4j.Plural
import com.tyler.mealmaker.R
import com.tyler.mealmaker.entities.Item
import com.tyler.mealmaker.entities.UnitOfMeasure
import com.tyler.mealmaker.extensions.*
import kotlinx.android.synthetic.main.add_item_modal.view.*

class AddItemModal(
    context: Context,
    builder: AlertDialog = AlertDialog.Builder(context).create(),
    block: (View, Item) -> Unit
) : LinearLayout(context, null, 0) {
    private val layoutInflater = LayoutInflater.from(context)

    init {
        layoutInflater.inflate(R.layout.add_item_modal, this)
        unit.setOptions(context, UnitOfMeasure.plurals)

        count.onChangeListener { _, new ->
            val unitOfMeasure = getSelectedMeasurement()
            if (new.toIntOrNull()?.equals(1) == true) {
                unit.setOptions(context, UnitOfMeasure.singulars)
                unit.setText(unitOfMeasure.singular)
            }
            else {
                unit.setOptions(context, UnitOfMeasure.plurals)
                unit.setText(unitOfMeasure.plural)
            }
        }

        builder.setButton("Add") { _, _ ->
            val item = Item(name.textString.pluralize(), count.textNumber, getSelectedMeasurement())

            val row = layoutInflater.inflate(R.layout.main_row, null).apply {
                fun Int.setup(text: String) = findViewById<TextView>(this).apply {
                    setMargin(top = 25)
                    this.text = text
                }
                R.id.left.setup(item.name)
                R.id.right.setup("${item.count} ${item.pluralizedUnit}")
            }
            block(row, item)
        }

        builder.setView(this, 70, 70, 70, 0)
        builder.show()
    }

    private fun getSelectedMeasurement() = UnitOfMeasure.get(unit.selectedItem.toString())!!
}