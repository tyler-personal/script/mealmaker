package com.tyler.mealmaker.helpers

import android.support.design.widget.Snackbar
import android.view.View
import com.tyler.mealmaker.extensions.setMargin

fun makeSnackbar(parentView: View, itemName: String) {
    Snackbar.make(parentView, "$itemName Added", Snackbar.LENGTH_LONG)
        .apply { view.setMargin(left = 300, right = 300, bottom = parentView.height + 40) }
        .show()
}